package com.devcamp.arrayintegerfilter.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayFilterInputService {
  int[] rainbows = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

  public ArrayList<Integer> getLargeNumber(int pos) {
    ArrayList<Integer> list = new ArrayList<>();
    for (int i = 0; i < rainbows.length; i++) {
      if (rainbows[i] > pos) {
        list.add(rainbows[i]);
      }
    }
    return list;
  }

  public int getIndexArray(int index) {
    int result = -1;

    if ((-1 < index) && (index < rainbows.length)) {
      result = rainbows[index];
    }

    return result;
  }
}
