package com.devcamp.arrayintegerfilter.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayintegerfilter.services.ArrayFilterInputService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ArrayIntegerFilterController {
  @Autowired
  private ArrayFilterInputService arrayFilterInputService;

  @GetMapping("/array-int-request-query/{pos}")
  public ArrayList<Integer> getLargeNumber(@PathVariable int pos) {
    return arrayFilterInputService.getLargeNumber(pos);
  }

  @GetMapping("/array-int-param/{index}")
  public int getIndexArray(@PathVariable int index) {
    return arrayFilterInputService.getIndexArray(index);
  }

}
